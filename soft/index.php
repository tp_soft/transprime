<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="../styles/tp_main_styles.css" />
<link rel="stylesheet" href="../styles/dropdownmenu.css" />
<link href="../themes/1/js-image-slider.css" rel="stylesheet" type="text/css" />
    <script src="../themes/1/js-image-slider.js" type="text/javascript"></script>
    <link href="themes/generic.css" rel="stylesheet" type="text/css" />

	<!-- Bullet Navigator Style -->
    <style>
        /* jssor slider bullet navigator skin 21 css */
        /*
            .jssorb21 div           (normal)
            .jssorb21 div:hover     (normal mouseover)
            .jssorb21 .av           (active)
            .jssorb21 .av:hover     (active mouseover)
            .jssorb21 .dn           (mousedown)
            */
        .jssorb21 div, .jssorb21 div:hover, .jssorb21 .av {
            background: url(../img/b21.png) no-repeat;
            overflow: hidden;
            cursor: pointer;
        }

        .jssorb21 div {
            background-position: -5px -5px;
        }

            .jssorb21 div:hover, .jssorb21 .av:hover {
                background-position: -35px -5px;
            }

        .jssorb21 .av {
            background-position: -65px -5px;
        }

        .jssorb21 .dn, .jssorb21 .dn:hover {
            background-position: -95px -5px;
        }
    </style>

    <!-- Arrow Navigator Style -->
    <style>
        /* jssor slider arrow navigator skin 21 css */
        /*
            .jssora21l              (normal)
            .jssora21r              (normal)
            .jssora21l:hover        (normal mouseover)
            .jssora21r:hover        (normal mouseover)
            .jssora21ldn            (mousedown)
            .jssora21rdn            (mousedown)
            */
        .jssora21l, .jssora21r, .jssora21ldn, .jssora21rdn {
            position: absolute;
            cursor: pointer;
            display: block;
            background: url(../img/a21.png) center center no-repeat;
            overflow: hidden;
        }

        .jssora21l {
            background-position: -3px -33px;
        }

        .jssora21r {
            background-position: -63px -33px;
        }

        .jssora21l:hover {
            background-position: -123px -33px;
        }

        .jssora21r:hover {
            background-position: -183px -33px;
        }

        .jssora21ldn {
            background-position: -243px -33px;
        }

        .jssora21rdn {
            background-position: -303px -33px;
        }
    </style>



    <!--
        Given a demo 'demos-jquery\full-width-slider.source.html'
        Go through following steps to transform jssor slider compatible with w3c standards and pass html validation.

        #1. Move styles inside 'head' tag
        #2. Add alt="" for all 'img' tag
        #3. Add 'data-' prefix for all custom attributes. e.g. u="image" -> data-u="image"
    -->

    <!-- it works the same with all jquery version from 1.x to 2.x -->
    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
    <!-- use jssor.slider.mini.js (40KB) instead for release -->
    <!-- jssor.slider.mini.js = (jssor.js + jssor.slider.js) -->
    <script type="text/javascript" src="../js/jssor.js"></script>
    <script type="text/javascript" src="../js/jssor.slider.js"></script>
    <script type="text/javascript" src="/transprime/scripts/slider_script.js"></script>


</head>

<body>
	<div style="background-color: #06C; min-height:100px; width:100%; border-radius:6px 3px 6px 2px">
    </div>
    
    
    
    	<!-- TOP DROP DOWN MENU CODE STARTS HERE -->
<div style="margin-top:-50px">
	<nav>
    <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">WordPress</a>
        <!-- First Tier Drop Down -->
        <ul>
            <li><a href="#">Themes</a></li>
            <li><a href="#">Plugins</a></li>
            <li><a href="#">Tutorials</a></li>
        </ul>        
        </li>
        <li><a href="#">Web Design</a>
        <!-- First Tier Drop Down -->
        <ul>
            <li><a href="#">Resources</a></li>
            <li><a href="#">Links</a></li>
            <li><a href="#">Tutorials</a>
        	<!-- Second Tier Drop Down -->
            <ul>
                <li><a href="#">HTML/CSS</a></li>
                <li><a href="#">jQuery</a></li>
                <li><a href="#">Other</a></li>
            </ul>
            </li>
        </ul>
        </li>
        <li><a href="#">Graphic Design</a></li>
        <li><a href="#">Inspiration</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="#">About</a></li>
    </ul>
</nav>
</div>
<!-- TOP DROPDOWN MENU CODE ENDS HERE -->
   <!-- 
        -->
    
    <img src="/transprime/images/box1bottom.png" style="width:100%;" />
 
 
 
 <div style="background-image:url(/transprime/images/wallback.png); width:100%; min-height:700px; border-radius: 6px 3px 6px 2px;">   
    <!-- Jssor Slider Begin -->
    <!-- You can move inline styles to css file or css block. -->
    <div id="slider1_container" style="position: relative; margin: 0 auto;
        top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; border-radius:6px 3px 6px 2px">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
                top: 0px; left: 0px; width: 100%; height: 100%;">
            </div>
            <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
                top: 0px; left: 0px; width: 100%; height: 100%;">
            </div>
        </div>
        <!-- Slides Container -->
        <div data-u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1300px;
            height: 500px; overflow: hidden;">
            <div>
                <img data-u="image" src="../img/1920/red.jpg" alt="" />
                <div data-u="caption" data-t="NO" data-t3="RTT|2" data-r3="137.5%" data-du3="3000" data-d3="500" style="position: absolute; width: 445px; height: 300px; top: 100px; left: 600px;">
                    <img src="../img/new-site/c-phone.png" style="position: absolute; width: 445px; height: 300px; top: 0px; left: 0px;" alt="" />
                    <img data-u="caption" data-t="CLIP|LR" data-du="4000" data-t2="NO" src="../img/new-site/c-jssor-slider.png" style="position: absolute; width: 102px; height: 78px; top: 70px; left: 130px;" alt="" />
                    <img data-u="caption" data-t="ZMF|10" data-t2="NO" src="../img/new-site/c-text.png" style="position: absolute; width: 80px; height: 53px; top: 153px; left: 163px;" alt="" />
                    <img data-u="caption" data-t="RTT|10" data-t2="NO" src="../img/new-site/c-fruit.png" style="position: absolute; width: 140px; height: 90px; top: 60px; left: 220px;" alt="" />
                    <img data-u="caption" data-t="T" data-du="4000" data-t2="NO" src="../img/new-site/c-navigator.png" style="position: absolute; width: 200px; height: 155px; top: 57px; left: 121px;" alt="" />
                </div>
                <div data-u="caption" data-t="RTT|2" data-r="-75%" data-du="1600" data-d="2500" data-t2="NO" style="position: absolute; width: 470px; height: 220px; top: 120px; left: 650px;">
                    <img src="../img/new-site/c-phone-horizontal.png" style="position: absolute; width: 470px; height: 220px; top: 0px; left: 0px;" alt="" />
                    <img data-u="caption" data-t3="MCLIP|L" data-du3="2000" src="../img/new-site/c-slide-1.jpg" style="position: absolute; width: 379px; height: 213px; top: 4px; left: 45px;" alt="" />
                    <img data-u="caption" data-t="MCLIP|R" data-du="2000" data-t2="NO" src="../img/new-site/c-slide-3.jpg" style="position: absolute; width: 379px; height: 213px; top: 4px; left: 45px;" alt="" />
                    <img data-u="caption" data-t="RTTL|BR" data-x="500%" data-y="500%" data-du="1000" data-d="-3000" data-r="-30%" data-t3="L" data-x3="70%" data-du3="1600" src="../img/new-site/c-finger-pointing.png" style="position: absolute; width: 257px; height: 300px; top: 80px; left: 200px;" alt="" />
                    <img src="../img/new-site/c-navigator-horizontal.png" style="position: absolute; width: 379px; height: 213px; top: 4px; left: 45px;" alt="" />
                </div>
                <div style="position: absolute; width: 480px; height: 120px; top: 30px; left: 30px; padding: 5px;
                    text-align: left; line-height: 60px; text-transform: uppercase; font-size: 50px;
                        color: #FFFFFF;">Touch Swipe Slider
                </div>
                <div style="position: absolute; width: 480px; height: 120px; top: 300px; left: 30px; padding: 5px;
                    text-align: left; line-height: 36px; font-size: 30px;
                        color: #FFFFFF;">
                        Build your slider with anything, includes image, content, text, html, photo, picture
                </div>
            </div>
            <div>
                <img data-u="image" src="../img/1920/purple.jpg" alt="" />
                <div style="position: absolute; width: 480px; height: 120px; top: 30px; left: 30px; padding: 5px;
                    text-align: left; line-height: 60px; text-transform: uppercase; font-size: 50px;
                        color: #FFFFFF;">Touch Swipe Slider
                </div>
                <div style="position: absolute; width: 480px; height: 120px; top: 300px; left: 30px; padding: 5px;
                    text-align: left; line-height: 36px; font-size: 30px;
                        color: #FFFFFF;">
                        Build your slider with anything, includes image, content, text, html, photo, picture
                </div>
            </div>
            <div>
                <img data-u="image" src="../img/1920/blue.jpg" alt="" />
                <div style="position: absolute; width: 480px; height: 120px; top: 30px; left: 30px; padding: 5px;
                    text-align: left; line-height: 60px; text-transform: uppercase; font-size: 50px;
                        color: #FFFFFF;">Touch Swipe Slider
                </div>
                <div style="position: absolute; width: 480px; height: 120px; top: 300px; left: 30px; padding: 5px;
                    text-align: left; line-height: 36px; font-size: 30px;
                        color: #FFFFFF;">
                        Build your slider with anything, includes image, content, text, html, photo, picture
                </div>
            </div>
        </div>
                
        <!-- Bullet Navigator Skin Begin -->
        <!-- bullet navigator container -->
        <div data-u="navigator" class="jssorb21" style="position: absolute; bottom: 26px; left: 6px;">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="POSITION: absolute; WIDTH: 19px; HEIGHT: 19px; text-align:center; line-height:19px; color:White; font-size:12px;"></div>
        </div>
        <!-- Bullet Navigator Skin End -->

        <!-- Arrow Navigator Skin Begin -->
        <!-- Arrow Left -->
        <span data-u="arrowleft" class="jssora21l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span data-u="arrowright" class="jssora21r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
        <!-- Arrow Navigator Skin End -->
        <a style="display: none" href="http://www.jssor.com">jQuery Carousel</a>
    </div>
    <div style="float:left; min-width:500px; min-height:100px; background-color:#FFF; margin:20px; padding:50px;">
    </div>
	<div style="min-width:200px; min-height:100px; background-color: #000; margin:20px; padding:50px">
    </div>
    
    </div>
        <div style="min-height:200px; background-color: #333; border-radius: 6px 3px 6px 2px; width:100%; vertical-align:baseline; margin-bottom:-100px;">
    </div>
</body>
</html>