<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HOME :: TRANSFORMATION PRIME</title>
<link rel="stylesheet" href="/transprime/styles/tp_main_styles.css" />
<link rel="stylesheet" href="/transprime/styles/dropdownmenu.css" />
</head>

<body>

<div style="width:100%; height:100px; background-color:#FFF;">
	<img src="/transprime/images/tp2.png" style="height:90px" />
    <div style="float:right; word-spacing:10px">
    	<a href=""><img src="/transprime/images/youtube.png" /></a>
        <a href=""><img src="/transprime/images/twitter.png" /></a>
        <a href=""><img src="/transprime/images/google-16.png" /></a>
        <a href=""><img src="/transprime/images/facebook.png" /></a>
        <a href=""><img src="/transprime/images/rss.png" /></a>
        <a href=""><img src="/transprime/images/logo_linkedin.gif" /></a>
    </div>
</div>
<!-- TOP DROP DOWN MENU CODE STARTS HERE -->
<div style="margin-top:-50px">
	<nav>
    <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">WordPress</a>
        <!-- First Tier Drop Down -->
        <ul>
            <li><a href="#">Themes</a></li>
            <li><a href="#">Plugins</a></li>
            <li><a href="#">Tutorials</a></li>
        </ul>        
        </li>
        <li><a href="#">Web Design</a>
        <!-- First Tier Drop Down -->
        <ul>
            <li><a href="#">Resources</a></li>
            <li><a href="#">Links</a></li>
            <li><a href="#">Tutorials</a>
        	<!-- Second Tier Drop Down -->
            <ul>
                <li><a href="#">HTML/CSS</a></li>
                <li><a href="#">jQuery</a></li>
                <li><a href="#">Other</a></li>
            </ul>
            </li>
        </ul>
        </li>
        <li><a href="#">Graphic Design</a></li>
        <li><a href="#">Inspiration</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="#">About</a></li>
    </ul>
</nav>
</div>
<!-- TOP DROPDOWN MENU CODE ENDS HERE -->

<div style="background: url(/transprime/images/bg_large.jpg) no-repeat; width:100%; height:900px">
	<div style="width:100%; height:600px; background-color:#FFF; vertical-align:middle; padding-top:100px; padding-right:100px; padding-top:150px;">
    	<div style="background-color:#000; width:80%; height:inherit; padding-left:10%; padding-right:10%; margin-left:50px;" align="center">
        </div>
        
    </div>
</div>

</body>
</html>